# Americatel Official Public Docker Image for WSO2 API Manager Analytics

## How to build an image and run
### 1. Checkout this repository into your local machine using the following git command.
```
git clone https://gitlab.com/docker.public/wso2am-analytics.git
```

### 2. Build the Docker image.
```
docker build -t registry.gitlab.com/docker.public/wso2am-analytics:2.2.0 .
```